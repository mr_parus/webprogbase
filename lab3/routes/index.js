var express = require('express');
var router = express.Router();
var fs = require("fs");
var Values = function(obj){
  var res = [];
  for(var key in obj){
    if(obj.hasOwnProperty(key))
      res.push(obj[key]);
  }
  return res;
};
var temp = Values(JSON.parse(fs.readFileSync("./data.json", "utf8")));


/* GET home page. */
router.get('/Home', function(req, res, next) {
  res.render('Home', { title: 'Express' });
});
router.get('/Books', function(req, res, next) {
  res.render('Books', { obj  : temp });
});
router.get('/Book:id', function(req, res, next) {
  res.render('Book', {
    obj: temp[req.params.id-1],
    nomer: req.params.id-1
  });
});

module.exports = router;
