var  net = require("net");
var server = net.createServer();
var fs = require("fs");
var colors = require('colors');
var connections =[];


var check = function(data){
    for(var key in connections)
        if (connections[key]!=data)return key;
    return true;
};

// Object.prototype.values = function(obj){
//     var res = [];
//     for(var key in obj){
//         if(obj.hasOwnProperty(key))
//             res.push(obj[key]);
//     }
//     return res;
// };
Values = function(obj){
    var res = [];
    for(var key in obj){
        if(obj.hasOwnProperty(key))
            res.push(obj[key]);
    }
    return res;
};
//var file = JSON.parse(fs.readFileSync("./data.json", "utf8"));

// fs.readFile(file,'utf8',function(err,data) {
//     if (err) { console.log('Error: ' + err);
//         return; }
//         console.log(JSON.parse(data));
// });

// var  get_connections_info= function(){
//
// };

server.on("connection", (socket)=>{
    var remoteAddress = socket.remoteAddress + ":" + socket.remotePort;
    if (check(remoteAddress)) connections.push(remoteAddress);
    console.log("new client connection is made %s".green, remoteAddress);
    socket.on("data",(data)=>{
        console.log("Data from %s: %s".cyan,remoteAddress,data);
        switch (data.toString().trim()){
            case "get all": {
                socket.write(fs.readFileSync("./data.json", "utf8"));
                break;
            }
            case "get by name":{
                let temp = Values(JSON.parse(fs.readFileSync("./data.json", "utf8")));
                temp = temp.sort((a,b)=>{
                    if (a.name < b.name)
                        return -1;
                    if (a.name > b.name)
                        return 1;
                    return 0;
                });
                socket.write("Server:\n"+JSON.stringify(temp, 8, " "));
             break;
            }
            case "get number of objects":{
                let temp = Object.Values(JSON.parse(fs.readFileSync("./data.json", "utf8")));
                socket.write("Server:\n"+temp.length);
                break;
            }
            case "get connections info":{
                socket.write("Server:");
                for(var key in connections){
                   if (connections[key]) socket.write(connections[key].toString()+"\n");
                }
                break;
            }
            default:{
                socket.write("Server: Unknown message! Use 'help'.");
                break;
            }
        }
    });
    socket.once("close",()=>{
        console.log("connection from %s closed".yellow,remoteAddress);
        connections[check(remoteAddress)]=null;
    });
    socket.on("error",(err)=>{
        console.log("error has occuped from %s ".red,remoteAddress,err.message);
        connections[check(remoteAddress)]=null;
    });
});

server.listen(9001,"127.0.0.1",()=>{
 console .log("server listening to %j",server.address())
});
