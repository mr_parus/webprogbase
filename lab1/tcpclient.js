const net = require('net');
const stdin = process.openStdin();

var status = null;
var adress;
console.log("Client: Enter 'connect' 'server address' to connect to server:");
stdin.addListener('data', (data) => {
    let str = data.toString().trim();
    switch (str) {
        // when client disconnected
        case "disconnect":{
            adress="";
            if (!status) break;
            status.end();
            console.log('Client: Disconnected from server!');
            status = null;
            break;
        }
        case "get all":
        case "get by name":
        case "get number of objects":
        case "get connections info ":
        default: {
            if (status) {
                status.write(str);  // send input string to server
            }
            if (str.substr(0, 7) == "connect") {
                if (!status){
                     if (((str.substr(8, 22)=="localhost:9000" )||(str.substr(8, 22)=="127.0.0.1:9000")||(str.substr(8.9)=="0"))) {
                         adress = str.substr(8, 22);
                         status = net.connect({host: "127.0.0.1", port: 9000}, () => {
                            console.log('Client: Connected to "%s".',adress);
                         });
                         status.on('data', (data) => {// receive data from server
                             console.log(data.toString());
                         });
                     }else if (((str.substr(8, 22)=="localhost:9001" )||(str.substr(8, 22)=="127.0.0.1:9001")||(str.substr(8.9)=="1"))) {
                        adress = str.substr(8, 22);
                        status = net.connect({host: "127.0.0.1", port: 9001}, () => {
                            console.log('Client: Connected to "%s".',adress);
                        });
                        status.on('data', (data) => {// receive data from server
                            console.log(data.toString());
                        });
                    }else console.log("Client: Unknown adrress! Enter 'connect' 'server address' to connect to server.");
                }else console.log("Client: Already connected!");
                break;
            }
            if (!status) console.log("Client: Unknown command! ")
        }
    }
});

