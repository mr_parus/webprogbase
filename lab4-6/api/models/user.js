var mongoose = require('./../services/dbConnection'),
    Schema = mongoose.Schema,
    User = new Schema({
        firstname: {
            type: String,
            required: true
        },
        lastname: {
            type: String,
            required: true
        },
        username: {
            type: String,
            required: true,
            index: {
                unique: true
            }
        },
        password: {
            type: String,
            required: true
        },
        status: {
            type: String,
            default: ""
        },
        favorites: {
            type: [{
                type: Schema.Types.ObjectId,
                ref: 'Post'
            }],
            default: []
        },
        ownPosts: {
            type: [{
                type: Schema.Types.ObjectId,
                ref: 'Post'
            }],
            default: []
        }
    });

module.exports = mongoose.model("User", User);