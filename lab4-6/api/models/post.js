var mongoose = require('./../services/dbConnection'),
    log = require('./../services/log')(module),
    Schema = mongoose.Schema,
    Post = new Schema({
        title: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        }
    });

Post.index({title: "text", description: "text"});

module.exports = mongoose.model("Post", Post);
