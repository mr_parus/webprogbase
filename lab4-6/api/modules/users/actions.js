var express = require('express'),
    router = express.Router(),
    log = require("./../../services/log")(module),
    config = require('./../../../config'),
    handlers = require('./handlers'),
    util = require('util');

/**
 * Get all users
 */
router.get('/', function (req, res, next){
    handlers.getAllUsers()
        .then(function(users){
            res
                .status(200)
                .send({
                    success: true,
                    users: users
                });
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
});

/**
 * Get user by id
 */
router.get('/:id', function (req, res, next){
    handlers.getUserById(req.params.id)
        .then(function(user){
            res
                .status(200)
                .send({
                    success: true,
                    user: user
                });
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
});

/**
 * Create user
 */
router.post('/', function (req, res, next){
    debugger;
    handlers.createUser(req.query)
        .then(function(){
            res
                .status(200)
                .send({
                    success: true,
                    msg: "New user saved"
                });
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
});

/**
 * Change user data
 */
router.post('/:id', function (req, res, next){
    var param = req.body ;
debugger;
    handlers.changeUserData(req.params.id, param)
        .then(function(){
            res
                .status(200)
                .send({
                    success: true,
                    msg: "User data changed"
                });
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
});

/**
 * Add some post to favorites
 */
router.post('/:id/favorites', function (req, res, next){
    handlers.addToFavorites(req.params.id, req.query.postId)
        .then(function(){
            res
                .status(200)
                .send({
                    success: true,
                    msg: "Post added to favorites"
                });
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
});

module.exports = router;