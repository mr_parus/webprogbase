var express = require('express'),
    router = express.Router(),
    log = require("./../../services/log")(module),
    config = require('./../../../config'),
    util = require('util'),
    handlers = require('./handlers');

/**
 * Get all posts
 */
router.get('/', function (req, res, next){
    handlers.getAllPosts()
        .then(function(posts){
            res
                .status(200)
                .send({
                    success: true,
                    posts: posts
                });
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
});

/**
 * Get post by id
 */
router.get('/:id', function (req, res, next){
    handlers.getPostById(req.params.id)
        .then(function(post){
            res
                .status(200)
                .send({
                    success: true,
                    post: post
                });
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
});





/**
 * Create new post
 */
router.post('/', function (req, res, next){
    debugger;
    handlers.createNewPost(req.body)
        .then(function(){
            res
                .status(200)
                .send({
                    success: true,
                    msg: "New post has been saved"
                });
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
});

/**
 * Search
 */
router.post('/search', function (req, res, next){
    debugger;
    if (req.query.str){
    handlers.search(req.query.str)
        .then(function(result){
            res
                .status(200)
                .send({
                    success: true,
                    result: result
                });
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
    }
    else {
        handlers.getAllPosts()
            .then(function(posts){
                res
                    .status(200)
                    .send({
                        success: true,
                        posts: posts
                    });
            },function(err){
                res
                    .status(err.httpStatus)
                    .send({
                        success: false,
                        message: err.message
                    });
            });
    }
});

/**
 * Update post by id
 */
router.post('/:id', function (req, res, next){
    debugger;
    handlers.updatePost(req.params.id, req.body)
        .then(function(){
            res
                .status(200)
                .send({
                    success: true,
                    msg: "Post has been updated"
                });
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
});

/**
 * Delete post by id
 */
router.delete('/:id', function (req, res, next){
    handlers.deletePost(req.params.id)
        .then(function(){
            res
                .status(200)
                .send({
                    success: true,
                    msg: "Post has been removed"
                });
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
});

/**
 * Like post by id
 */
router.post('/:id/like', function (req, res, next){
    debugger;
    handlers.likePost(req.params.id, req.body.userId)
        .then(function(){
            res
                .status(200)
                .send({
                    success: true,
                    msg: "Post has been liked or disliked"
                });
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
});


module.exports = router;