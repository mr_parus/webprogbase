var Post = require('./../../../models/post'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');

module.exports = updatePost;

/**
 * Create new post
 * @param postData - compleate post data
 * @returns {Promise<T>}
 */
function updatePost(postId, postData){
    var defer = q.defer(),
        error;
    debugger;
    Post
        .where({_id: postId})
        .update({
            $set: postData
        })
        .then(function(post){
            defer.resolve();
        }, function(err){
            error = new HttpError(404, 'Post not found');
            log.error(error.toString());
            defer.reject(error);
        });

    return defer.promise;
}