module.exports = {
    getAllPosts: require('./getAllPosts'),
    getPostById: require('./getPostById'),
    createNewPost: require('./createNewPost'),
    updatePost: require('./updatePost'),
    deletePost: require('./deletePost'),
    search: require('./searchInPosts')
};