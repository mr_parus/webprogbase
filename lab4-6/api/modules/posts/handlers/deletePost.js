var Post = require('./../../../models/post'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');

module.exports = deletePost;

/**
 * Find post by his id adn delete
 * @param postId - post id from req
 * @returns {Promise<T>}
 */
function deletePost(postId){
    var defer = q.defer(),
        error;

    Post
        .findOne({
            _id: postId
        })
        .exec()
        .then(function(post){
            post.remove(function(err){
                if(!err){
                    defer.resolve();
                } else {
                    error = new HttpError(503, 'Service unavailable');
                    log.error(error.toString());
                    defer.reject(error);
                }
            });
        }, function(err){
            error = new HttpError(503, 'Service unavailable');
            log.error(error.toString());
            defer.reject(error);
        });

    return defer.promise;
}