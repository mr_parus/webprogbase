module.exports = {
    getAllUsers: require('./getAllUsers'),
    createUser: require('./createUser'),
    changeUserData: require('./changeUserData'),
    getUserById: require('./getUserById'),
    addToFavorites: require('./addToFavorite'),
    countUsers: require('./countUsers')
};