var User = require('./../../../models/user'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');

module.exports = addToFavorites;

/**
 * Get all posts form db
 * @returns {Promise<T>}
 */
function addToFavorites(userId, postId){
    var defer = q.defer(),
        error;

    if(checkObjectId(userId) && checkObjectId(postId)){
        User
            .where({
                _id: userId
            })
            .update({
                $push: { favorites: postId}
            })
            .exec()
            .then(function(){
                defer.resolve();
            }, function(err){
                error = new HttpError(404, 'User not found');
                log.error(error.toString());
                defer.reject(error);
            });
    } else {
        error = new HttpError(406, 'Not acceptable id value');
        log.error(error.toString());
        defer.reject(error);
    }

    return defer.promise;
}

function checkObjectId(id){
    if ((id+'').match(/^[0-9a-fA-F]{24}$/))
        return true;

    return false;
}