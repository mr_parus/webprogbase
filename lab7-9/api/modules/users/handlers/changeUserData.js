var User = require('./../../../models/user'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q'),
    config = require('./../../../../config'),
    crypto = require('crypto');

module.exports = changeUserData;

/**
 * Create new user
 * @param userData - new user data
 * @returns {Promise<T>}
 */
function changeUserData(userId, userData){
    var defer = q.defer(),
        error;
    debugger;

    isUsernameUnique(userData.username)
        .then(function(){
            if (userData.password)
                userData.password = hashPassword(userData.password);

            return User
                .where({ _id: userId })
                .update({ $set: userData })
                .exec();
        }, defer.reject)
        .then(function(){
            defer.resolve();
        }, function(err){
            error = new HttpError(503, 'Service unavailable');
            log.error(error.toString());
            defer.reject(error);
        });

    return defer.promise;
}


/**
 * Hash user's password
 * @param password - user's password
 * @returns {Buffer|string}
 */
function hashPassword(password) {
    return crypto
        .createHash('md5')
        .update(password + config.hashSecret)
        .digest("hex");
}

/**
 * Check unicity of username
 * @param username - username of new user
 * @returns {Promise<T>}
 */
function isUsernameUnique(username){
    var defer = q.defer(),
        error;

    User
        .find({username: username})
        .exec()
        .then(function(docs){
            if(docs.length){
                error = new HttpError(409, 'User is already exist');
                log.error(error.toString());
                defer.reject(error);
            } else {
                defer.resolve();
            }
        }, function(err){
            error = new HttpError(503, 'Service unavailable');
            log.error(error.toString());
            defer.reject(error);
        });

    return defer.promise;
}