var User = require('./../../../models/user'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');

module.exports = getAllUsers;

/**
 * Get all users form db
 * @returns {Promise<T>}
 */
function getAllUsers(){
    var defer = q.defer(),
        error;

    User
        .find({})
        .exec()
        .then(function(userArr){
            defer.resolve(userArr);
        }, function(err){
            error = new HttpError(503, 'Service unavailable');
            log.error(error.toString());
            defer.reject(error);
        });

    return defer.promise;
}