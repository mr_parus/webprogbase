var User = require('./../../../models/user'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q'),
    config = require('./../../../../config'),
    crypto = require('crypto');

module.exports = createUser;

/**
 * Create new user
 * @param userData - new user data
 * @returns {Promise<T>}
 */
function createUser(userData){
    var defer = q.defer(),
        error;
    isUsernameUnique(userData.username)
        .then(function(){
            new User({
                    firstname: userData.firstname,
                    lastname: userData.lastname,
                    gender: userData.gender,
                    username: userData.username,
                    password: hashPassword(userData.password)
                })
                .save()
                .then(function(){
                    defer.resolve();
                }, function(err){
                    error = new HttpError(503, 'Service unavailable');
                    log.error(error.toString());
                    defer.reject(error);
                });
        }, defer.reject);
    return defer.promise;
}

/**
 * Check unicity of username
 * @param username - username of new user
 * @returns {Promise<T>}
 */
function isUsernameUnique(username){
    var defer = q.defer(),
        error;

    User
        .find({username: username})
        .exec()
        .then(function(docs){
            if(docs.length){
                error = new HttpError(409, 'Пользователь с таким логином уже есть');
                log.error(error.toString());
                defer.reject(error);
            } else {
                defer.resolve();
            }
        }, function(err){
            error = new HttpError(503, 'Service unavailable');
            log.error(error.toString());
            defer.reject(error);
        });

    return defer.promise;
}

/**
 * Hash user's password
 * @param password - user's password
 * @returns {Buffer|string}
 */
function hashPassword(password) {
    return crypto
        .createHash('md5')
        .update(password + config.hashSecret)
        .digest("hex");
}
