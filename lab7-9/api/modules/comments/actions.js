var express = require('express'),
    router = express.Router(),
    log = require("./../../services/log")(module),
    config = require('./../../../config'),
    handlers = require('./handlers');

/**
 * Create new comment
 */
router.post('/', function(req, res, next){
    debugger;
    handlers.postNewComment(req.body)
        .then(function(){
            res
                .status(200)
                .send({
                    success: true,
                    msg: "New comment has been saved"
                });
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
});

/**
 * Delete some comment
 */
router.delete('/:id', function(req, res, next){
    handlers.deleteComment(req.params.id)
        .then(function(){
            res
                .status(200)
                .send({
                    success: true,
                    msg: "Comment has been removed"
                });
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
});

module.exports = router;