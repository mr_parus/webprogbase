var Comment = require('./../../../models/comment'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');


module.exports = deleteComment;

/**
 * Find comment by his id adn delete
 * @param commentId - comment id from req
 * @returns {Promise<T>}
 */
function deleteComment(commentId){
    var defer = q.defer(),
        error;
    Comment
        .findOne({
            _id: commentId
        })
        .exec()
        .then(function(comment){
            comment.remove(function(err){
                if(!err){
                    debugger;
                    defer.resolve();
                } else {
                    debugger;
                    error = new HttpError(503, 'Service unavailable');
                    log.error(error.toString());
                    defer.reject(error);
                }
            });
        }, function(err){
            debugger;
            error = new HttpError(503, 'Service unavailable');
            log.error(error.toString());
            defer.reject(error);
        });

    return defer.promise;
}