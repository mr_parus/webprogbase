var Comment = require('./../../../models/comment'),
    Post = require('./../../../models/post'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');

module.exports = createNewComment;

/**
 * Create new comment
 * @param commentData - compleate comment data
 * @returns {Promise<T>}
 */
function createNewComment(commentData){
    var defer = q.defer(),
        error;

    new Comment({
        text: commentData.text,
        authorId: commentData.authorId,
        postId: commentData.postId
    })
        .save()
        .then(function(doc){
            Post
                .where({ _id: doc.postId })
                .update({$push: {comments: doc._id}})
                .exec()
                .then(defer.resolve,
                    function(err){
                        error = new HttpError(503, 'Service unavailable');
                        log.error(error.toString());
                        defer.reject(error);
                    });
        }, function(err){
            error = new HttpError(503, 'Service unavailable');
            log.error(error.toString());
            defer.reject(error);
        });
    return defer.promise;
}
