var Post= require('./../../../models/post'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');

module.exports = likePost;

/**
 * Get all posts form db
 * @returns {Promise<T>}
 */
function likePost(postId, userId){
    var defer = q.defer(),
        error;

    Post.update({
            _id: postId
        },{
            $push: { likes: userId}
        })
        .then(function(){
            defer.resolve();
        }, function(err){
            error = new HttpError(404, 'Post not pound');
            log.error(error.toString());
            defer.reject(error);
        });

    if(checkObjectId(userId) && checkObjectId(postId)){
        Post
            .findOne({
                _id: postId
            })
            .exec()
            .then(function(doc){
                var likesArr = doc.likes,
                    authorLikeIndex = likesArr.indexOf(userId);

                    if(~authorLikeIndex){
                        likesArr.splice(authorLikeIndex, 1);
                    } else {
                        likesArr.push(userId);
                    }

                    return doc
                        .update({ $set: { likes: likesArr } })
                        .exec();
            }, function(err){
                error = new HttpError(404, 'User not found');
                log.error(error.toString());
                defer.reject(error);
            })
            .then(function(){
                defer.resolve();
            }, function(err){
                error = new HttpError(503, 'Service unavailable');
                log.error(error.toString());
                defer.reject(error);
            });
    } else {
        error = new HttpError(406, 'Not acceptable id value');
        log.error(error.toString());
        defer.reject(error);
    }

    return defer.promise;
}

function checkObjectId(id){
    if ((id+'').match(/^[0-9a-fA-F]{24}$/))
        return true;

    return false;
}