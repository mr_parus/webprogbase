module.exports = {
    getAllPosts: require('./getAllPosts'),
    getAllUsersPosts: require('./getAllUsersPosts'),
    getAllUsersFavoritesPosts: require('./getAllUsersFavoritesPosts'),
    getPostById: require('./getPostById'),
    createNewPost: require('./createNewPost'),
    updatePost: require('./updatePost'),
    likePost: require('./likePost'),
    deletePost: require('./deletePost'),
    search: require('./searchInPosts'),
    countPosts: require('./countPosts')
};