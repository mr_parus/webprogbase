var Post = require('./../../../models/post'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');

module.exports = getAllPosts;

/**
 * Get all posts form db
 * @returns {Promise<T>}
 */
function getAllPosts(){
    var defer = q.defer(),
        error;
    Post
        .find({})
        .populate({
            path: 'comments',
            populate: {
                path: 'authorId'
            }
        })
        .exec()
        .then(function(postsArr){
            defer.resolve(postsArr);
        }, function(err){
            error = new HttpError(503, 'No users');
            log.error(error.toString());
            defer.reject(error);
        });

    return defer.promise;
}
