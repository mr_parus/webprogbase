var User = require('./../../../models/user'),
    HttpError = require('./../../../services/httpError'),
    log = require('./../../../services/log')(module),
    q = require('q');

module.exports = getAllUsersPosts;

/**
 * Get all user's posts form db
 * @returns {Promise<T>}
 */
function getAllUsersPosts(userId){
    var defer = q.defer(),
        error;

    User
        .findOne({_id: userId})
        .populate({
            path: 'ownPosts',
            populate: {
                path: 'comments',
                populate: {
                    path: 'authorId'
                }
            }
        })
        .exec()
        .then(function(user){
            defer.resolve(user);
        }, function(err){
            error = new HttpError(503, 'Service unavailable');
            log.error(error.toString());
            defer.reject(error);
        });

    return defer.promise;
}