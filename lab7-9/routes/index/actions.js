var express = require('express'),
    router = express.Router(),
    config = require('./../../config/index'),
    crypto = require('crypto'),
    handlers1 = require('./../../api/modules/users/handlers'),
    handlers = require('./../../api/modules/posts/handlers');

module.exports = router;

//TODO  Error page= null
/**
 * Get main page
 */
router.get('/', function(req, res, next) {
    handlers1.getAllUsers()
        .then(function(userst){
            res
                .status(200)
                .render('index',{users:userst});
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
});


/**
 * Get user page by ID
 */
router.get('/users/:ID',function(req, res){
    handlers.getAllPosts()
        .then(function(posts){
            res
                .status(200)
                .render('index',{posts:posts});
        },function(err){
            res
                .status(err.httpStatus)
                .send({
                    success: false,
                    message: err.message
                });
        });
});



