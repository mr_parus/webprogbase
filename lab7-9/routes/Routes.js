var expressValidator = require('express-validator');

module.exports = createUrls;

/**
 * Set basic routes
 * @param app - express app
 */
function createUrls(app){
    app.use(expressValidator([]));
    app.use('/', require('./index/actions'));
    app.use('/', require('./users/actions'));
    app.use('/', require('./posts/actions'));
}