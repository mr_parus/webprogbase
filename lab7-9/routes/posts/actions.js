var express = require('express'),
    router = express.Router(),
    config = require('./../../config/index'),
    handlers = require('./../../api/modules/posts/handlers'),
    handlers1 = require('./../../api/modules/users/handlers'),
    bodyParser = require('body-parser');

module.exports = router;

var busboyBodyParser = require('busboy-body-parser');


router.use(bodyParser.json());
router.use(busboyBodyParser({ limit: '5mb' }));

/**
 * Create new post
 */
router.post('/posts/', function (req, res) {
   if (req.user){
       var files = [];

       for (var key in req.files.images){
           if (req.files.images.hasOwnProperty(key))
               files.push(req.files.images[key].data.toString('base64'));
       }

        var postData = {
            images:files,
            authorId: req.user._id.toString(),
            title: req.body.title,
            description: req.body.description
        };
       handlers.createNewPost(postData)
           .then(function(){
               res
                   .status(200)
                   .redirect('/users/'+req.user._id.toString())
           },function(err){
               res
                   .status(err.httpStatus)
                   .send({
                       success: false,
                       message: err.message
                   });
           });
   } else
    res.end("You are not Registered");
});
/**
 * Delete post
 */
router.get('/posts/delete/:ID', function (req, res) {
    if (req.user&&req.params.ID.toString() != req.user._id.toString()){
        handlers.deletePost(req.params.ID)
            .then(function(){
                res
                    .status(200)
                    .redirect('/users/'+req.user._id.toString())
            },function(err){
                res
                    .status(err.httpStatus)
                    .send({
                        success: false,
                        message: err.message
                    });
            });
    }else res.end("You can*t do this!");
});

/**
 * Get posts for page
 */
router.get("/page/:number",function (req,res) {
    debugger;
    if (req.params.number>0){
        handlers1.getAllUsers()
            .then(function(data){
                for (var i=6*(req.params.number-1),posts=[];i<(6*req.params.number);i++){
                    if (i<=data.length&&data[i]){posts.push(data[i])}else break;
                }
                debugger;
                res.send({
                    posts:posts
                });
            },function(err){
            });
    } else {
        res.send({
            posts:[]
        });
    }
});


